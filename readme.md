## Functions

<dl>
<dt><a href="#fromTheme">fromTheme(name)</a></dt>
<dd><p>Extracts a value from theme.</p>
</dd>
<dt><a href="#ifProp">ifProp(mapping)</a></dt>
<dd><p>Returns a value dependent on the presence of props.</p>
</dd>
<dt><a href="#ifPropEquals">ifPropEquals(mapping)</a></dt>
<dd><p>Returns a value dependent on the values of props.</p>
</dd>
<dt><a href="#fromProp">fromProp(path)</a></dt>
<dd><p>Digs through props and returns value.</p>
</dd>
</dl>

<a name="fromTheme"></a>

## fromTheme(name)
Extracts a value from theme.

**Kind**: global function  

| Param | Type | Description |
| --- | --- | --- |
| name | <code>string</code> | The key for the theme value to return. |

**Example**  
```js
// returns props.theme.colorGrey
styled.div`
 backgroundColor: ${fromTheme('colorGrey')};
`;
```
<a name="ifProp"></a>

## ifProp(mapping)
Returns a value dependent on the presence of props.

**Kind**: global function  

| Param | Type | Description |
| --- | --- | --- |
| mapping | <code>object</code> | An object that maps prop keys to strings or functions. |

**Example**  
```js
// returns 'red' if 'colored' prop is not falsey, and 'black' otherwise.
styled.div`
 backgroundColor: ${ifProp({
   colored: 'red',
   greyscale: 'grey',
   default: 'black',
 });
`;
```
<a name="ifPropEquals"></a>

## ifPropEquals(mapping)
Returns a value dependent on the values of props.

**Kind**: global function  

| Param | Type | Description |
| --- | --- | --- |
| mapping | <code>object</code> | An object that maps prop keys to strings or functions. |

**Example**  
```js
// returns '#ff0000' if 'color' prop equals 'red'.
styled.div`
  background: ${ifPropEquals({
    color: { red: '#ff0000', green: '#00800'},
    image: { happyCows: 'url(happyCows)' },
    default: fromTheme('black');
  });
`;
```
<a name="fromProp"></a>

## fromProp(path)
Digs through props and returns value.

**Kind**: global function  

| Param | Type | Description |
| --- | --- | --- |
| path | <code>string</code> | A string representation of the path of keys to parse. |

**Example**  
```js
// returns props.user.favoriteColors.green.
styled.div`
 backgroundColor: ${fromProp('user.favoriteColors.green')};
`;
```
