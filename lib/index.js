"use strict";

var fromTheme = function fromTheme(name) {
  return function (props) {
    return props.theme[name];
  };
};

var isFunction = function isFunction(possibleFunc) {
  return typeof possibleFunc === 'function';
};

var callIfFunction = function callIfFunction(possibleFunc, argument) {
  return isFunction(possibleFunc) ? possibleFunc(argument) : possibleFunc;
};

var firstValueFrom = function firstValueFrom(props, mapping) {
  var _arr = Object.keys(props);

  for (var _i = 0; _i < _arr.length; _i++) {
    var prop = _arr[_i];
    if (props[prop] && mapping[prop]) return mapping[prop];
  }

  return false;
};

var firstKeyValuePairFrom = function firstKeyValuePairFrom(props, mapping) {
  var _arr2 = Object.keys(props);

  for (var _i2 = 0; _i2 < _arr2.length; _i2++) {
    var prop = _arr2[_i2];
    if (props[prop] && mapping[prop]) return [prop, mapping[prop]];
  }

  return false;
};

var ifProp = function ifProp(mapping) {
  return function (props) {
    var firstValue = firstValueFrom(props, mapping) || mapping.default;
    return callIfFunction(firstValue, props);
  };
};

var ifPropEquals = function ifPropEquals(mapping) {
  return function (props) {
    var _firstKeyValuePairFro = firstKeyValuePairFrom(props, mapping),
        presentProp = _firstKeyValuePairFro[0],
        valueAssignment = _firstKeyValuePairFro[1];

    var value = presentProp && valueAssignment && valueAssignment[props[presentProp]] || mapping.default;
    return callIfFunction(value, props);
  };
};

var fromProp = function fromProp(path) {
  return function (props) {
    return path.split('.').reduce(function (accum, nextKey) {
      return accum && accum[nextKey];
    }, props);
  };
};

module.exports = {
  fromTheme: fromTheme,
  ifProp: ifProp,
  ifPropEquals: ifPropEquals,
  fromProp: fromProp
};
