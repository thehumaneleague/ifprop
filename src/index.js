

/**
 * Extracts a value from theme.
 * @param {string} name - The key for the theme value to return.
 * @example
 * // returns props.theme.colorGrey
 * styled.div`
 *  backgroundColor: ${fromTheme('colorGrey')};
 * `;
 */
const fromTheme = name => props => props.theme[name];

// Checks whether argument is a function.
const isFunction = possibleFunc => typeof possibleFunc === 'function';

// Calls argument if it is a function.
const callIfFunction = (possibleFunc, argument) => {
  return isFunction(possibleFunc) ? possibleFunc(argument) : possibleFunc;
};

// Retrieves the value of the first mapped prop (and calls if function).
// firstValueFrom({ propA, propB }, { propA: 12 }) returns 12.
const firstValueFrom = (props, mapping) => {
  for (const prop of Object.keys(props)) {
    if (props[prop] && mapping[prop]) return mapping[prop];
  }
  return false;
};

// Retrieves an array, where the first member is the first key shared by props and mapping,
// and the second member is the value of the mapping under that key.
const firstKeyValuePairFrom = (props, mapping) => {
  for (const prop of Object.keys(props)) {
    if (props[prop] && mapping[prop]) return [prop, mapping[prop]];
  }
  return false;
};

/**
 * Returns a value dependent on the presence of props.
 * @param {(object)} mapping - An object that maps prop keys to strings or functions.
 * @example
 * // returns 'red' if 'colored' prop is not falsey, and 'black' otherwise.
 * styled.div`
 *  backgroundColor: ${ifProp({
 *    colored: 'red',
 *    greyscale: 'grey',
 *    default: 'black',
 *  });
 * `;
 */
const ifProp = (mapping) => (
  (props) => {
    const firstValue = firstValueFrom(props, mapping) || mapping.default;
    return callIfFunction(firstValue, props);
  }
);

/**
 * Returns a value dependent on the values of props.
 * @param {(object)} mapping - An object that maps prop keys to strings or functions.
 * @example
 * // returns '#ff0000' if 'color' prop equals 'red'.
 * styled.div`
 *   background: ${ifPropEquals({
 *     color: { red: '#ff0000', green: '#00800'},
 *     image: { happyCows: 'url(happyCows)' },
 *     default: fromTheme('black');
 *   });
 * `;
 */
const ifPropEquals = (mapping) => (
  (props) => {
    const { 0: presentProp, 1: valueAssignment } = firstKeyValuePairFrom(props, mapping);
    const value = (presentProp && valueAssignment && valueAssignment[props[presentProp]]) || mapping.default;
    return callIfFunction(value, props);
  }
);

/**
 * Digs through props and returns value.
 * @param {(string)} path - A string representation of the path of keys to parse.
 * @example
 * // returns props.user.favoriteColors.green.
 * styled.div`
 *  backgroundColor: ${fromProp('user.favoriteColors.green')};
 * `;
 */
const fromProp = path => (
  props => path.split('.').
    reduce((accum, nextKey) => accum && accum[nextKey], props)
);

module.exports = { fromTheme, ifProp, ifPropEquals, fromProp };
