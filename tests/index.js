const { should } = require('chai');

const { fromTheme, ifProp, ifPropEquals, fromProp } = require('../src/index.js');

should();

describe('fromTheme', () => {
  it('gets values from theme', () => {
    const testProps = {
      theme: { color: 'red' },
    };

    fromTheme('color')(testProps).
      should.equal('red');
  });
});

describe('ifProp', () => {
  it('returns the appropriate value if prop is present', () => {
    const testProps = {
      fullWidth: 'fullWidth',
    };

    ifProp({ fullWidth: '100%' })(testProps).
      should.equal('100%');
  });

  it('calls appropriate value for a function if prop is present', () => {
    const testProps = {
      width: '20',
      padding: '10',
    };
    const widthAndPadding = (props) => parseInt(props.width) + parseInt(props.padding);

    ifProp({ width: widthAndPadding })(testProps).
      should.equal(30);
  });

  it('returns null if prop is absent and default not defined', () => {
    const testProps = {
      color: 'red',
    };

    should(ifProp({ fullWidth: '100%'} )(testProps)).
      not.exist;
  });

  it('returns default if prop is absent and default is defined', () => {
    const testProps = {
      color: 'red',
    };

    ifProp({ fullWidth: '100%', default: '50%' } )(testProps).
      should.equal('50%');
  });

  it('calls appropriate function if prop is absent and default is a function', () => {
    const testProps = {
      color: 'red',
      theme: { grey: 'rgb(50,50,50)' },
    };

    ifProp({ fullWidth: '100%', default: fromTheme('grey') } )(testProps).
      should.equal('rgb(50,50,50)');
  });
});

describe('ifPropEquals', () => {
  it('returns the appropriate value if prop is present and matches target', () => {
    const testProps = {
      color: 'red',
    };

    ifPropEquals({ color: { red: 'rgb(200,40,0)' } })(testProps).
      should.equal('rgb(200,40,0)');
  });

  it('returns null if prop is present and does not match target', () => {
    const testProps = {
      color: 'red',
    };

    should(ifPropEquals({ color: { blue: 'rgb(50,200,200)' } })(testProps)).
      not.exist;
  });

  it('calls appropriate function if prop is present and matches target', () => {
    const testProps = {
      color: 'grey',
      theme: { grey: 'rgb(50,50,50)' },
    };

    ifPropEquals({ color: { grey: fromTheme('grey') } })(testProps).
      should.equal('rgb(50,50,50)');
  });

  it('returns the default if prop is absent', () => {
    const testProps = {
      color: 'red',
    };

    ifPropEquals({
      width: { full: '100%' },
      default: '50%',
    })(testProps).
      should.equal('50%');
  });

  it('returns the default if prop is present and does not match', () => {
    const testProps = {
      color: 'red',
    };

    ifPropEquals({ color: { blue: 'rgb(50,200,200)' }, default: 'green' })(testProps).
      should.equal('green');
  });

  it('calls the default if prop is absent and default is function', () => {
    const testProps = {
      color: 'red',
      theme: { defaultWidth: '50%' },
    };

    ifPropEquals({
      width: { full: '100%' },
      default: fromTheme('defaultWidth'),
    })(testProps).
      should.equal('50%');
  });
});

describe('fromProp', () => {
  it('returns unnested prop values', () => {
    const testProps = {
      color: 'red',
    };

    fromProp('color')(testProps).
      should.equal('red');
  });

  it('returns nested prop values', () => {
    const testProps = {
      user: { favoriteColor: { hue: '120' } },
    };

    fromProp('user.favoriteColor.hue')(testProps).
      should.equal('120');
  });

  it('returns null if prop is absent', () => {
    const testProps = {
      color: 'red',
    };

    should(fromProp('width')(testProps)).
      not.exist;
  });
});
